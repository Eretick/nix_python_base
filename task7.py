#Дан список из строк. Используя join, соедините строки так, чтобы они были разделены через запятую. 
# На выходе должна получиться строка в виде "my_string1,my_string2,my_string3"

def join_strings(strings:str):
    return ','.join(strings)

if __name__ == '__main__':
    string = ['my_string1', 'my_string2', 'my_string3', 'my_string4','my_string5','my_string6']
    print(join_strings(string))