# Напишите функцию, которая принимает список, и число. Функция должна разбить список на N кусков, переданных в функцию в качестве втрого аргумента. 
# Выполнить проверки по здравому смыслу (например, нет смысла пытаться разбить список из 3 элементов на 4 элемента)
from math import ceil
def split_list(list:str, number:int):    
    lists = [list[i::number] for i in range(number)]
    return lists

if __name__ == '__main__':
    list = [1,2,3,4,5,6,7,8]
    lists1 = split_list(list,6)
    print(lists1)