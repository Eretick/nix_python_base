# Дан список из строк. Создайте однострочное решение (при помощи list comprehension), которое приведёт к верхнему регистру все строки, содержащие слово 'price')

def upper_strings(string:list):
    return [i.upper() for i in string]

if __name__ == '__main__':
    strings = ['String 1', 'String 2', 'String 3']
    upper = upper_strings(strings)
    print(upper)