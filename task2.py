# Напишите template строки, который можно будет многократно переиспользовать, вставляя в него имя и фамилию человека. Используйте метод строки "format".

def template_string(name, surname):
    return "Hello, {} {}".format(name,surname)

if __name__ == '__main__':
    name = input("Enter name:")
    surname = input("Enter surname:")
    template = template_string(name, surname)
    print(template)
