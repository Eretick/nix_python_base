#Создать функцию, которая принимает на вход два списка: первый — список, который нужно очистить от определённых значений, второй — список тех значений, от которых нужно очистить. 
# Например, list1 = [1, 2, 3, 4, 5], list2 = [1, 3, 4], функция должна вернуть [2, 5]

def clear_list(source_list:list, del_values:list):
    """This function deletes all values in del_values list from source_list  """
    for i in del_values:
        if i in source_list:
            source_list.remove(i)

if __name__ == '__main__':
    source = [1,2,3,4,5,6,7,10]
    del_list = [3,5,7,6]
    clear_list(source, del_list)
    print(source)