#Есть список из случайных чисел и строк. Создайте цикл, итерирующийся до тех пор, пока не встретится число "777". 
# Если в течении 100 попыток число не будет найдено — остановить цикл и вызвать ошибку с соответсвующим сообщением.

def find_777(list:str):
    tries = 0
    while tries < 100:
        print(list[tries])
        if list[tries] == 777 or list[tries]=='777':
            print('Found number 777 with {} tries!'.format(tries))
            return
        tries +=1
        
    else:
        raise Exception('Number 777 was not found!')

if __name__ == '__main__':
    import random
    values = [random.randint(1,800) for i in range(125)]
    values += [777]
    find_777(values)