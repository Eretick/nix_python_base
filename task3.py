# Напишите функцию, которая будет преобразовывать цену к формату, отображающему до двух знаков после точки, например:
# 22.32131 -> 22.32
# 58.60125 -> 58.6
# 34.0 -> 34

def prettify_price(price):
    new_numb = round(price,2)
    if new_numb % 1 == 0:
        new_numb = int(new_numb)
    return new_numb

#testing
if __name__ == '__main__':
    print(prettify_price(22.32131)) # -> 22.32
    print(prettify_price(58.60125)) # -> 58.6
    print(prettify_price(34.0))     # -> 34
